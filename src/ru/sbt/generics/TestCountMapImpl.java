package ru.sbt.generics;

import java.util.Map;

public class TestCountMapImpl {

    public static void main(String[] args) {
        CountMap<Integer> cm = new HashCountMapImpl<>();

        System.out.println("---------add--------");
        System.out.println(cm.toMap());
        cm.add(10);
        cm.add(5);
        cm.add(6);
        cm.add(10);
        cm.add(5);
        cm.add(10);
        System.out.println(cm.toMap());

        System.out.println("---------toMap--------");
        Map<Integer,Integer> map = cm.toMap();
        System.out.println(map);

        System.out.println("---------size--------");
        System.out.println(cm.size());

        System.out.println("----------remove------");
        System.out.println(cm.remove(10));
        System.out.println(cm.toMap());

        System.out.println("----------ToMap2------");
        cm.add(15);
        cm.add(5);
        cm.add(30);
        cm.toMap(map);
        System.out.println(map);
    }


}
