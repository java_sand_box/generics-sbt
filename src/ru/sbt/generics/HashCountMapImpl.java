package ru.sbt.generics;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

public class HashCountMapImpl<T> implements CountMap <T> {

    private HashMap<T, Integer> _instanceMap;

    public HashCountMapImpl() {
        _instanceMap = new HashMap<>();
    }
    public HashCountMapImpl(T firstObject) {
        _instanceMap = new HashMap<>();
        _instanceMap.put(firstObject, 1);
    }
    @Override
    public void add(T o) {
        add(new AbstractMap.SimpleEntry(o, 1));
    }
    private void add(Map.Entry<T,Integer> entry){
        T key = entry.getKey();
        _instanceMap.put( key , _instanceMap.containsKey(key) ? (_instanceMap.get(key) + entry.getValue()) : 1);
    }
    @Override
    public int getCount(T o) {
        return _instanceMap.get(o) == null ? 0 : _instanceMap.get(o);
    }
    @Override
    public int remove(T o) {
        return _instanceMap.containsKey(o) ? _instanceMap.remove(o) : 0 ;
    }
    @Override
    public int size() {
        return _instanceMap.size();
    }
    @Override
    public void addAll(CountMap <T> source) {
        source.toMap().entrySet().forEach(entry -> add(entry));
    }
    @Override
    public Map <T, Integer> toMap() {
        return new HashMap(_instanceMap);
    }

    @Override
    public void toMap(Map<T, Integer> destination) {
        destination.putAll(_instanceMap);
    }
}
